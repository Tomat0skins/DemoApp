#pragma once

#include <QObject>
#include <QString>

#include <QJsonDocument>

/**
 * @brief Singleton implementation of the ConfigFacade class used in C++ and QML
 *        This basic implentation uses a JSON file to store config values.
 */
class ConfigFacade : public QObject
{
    Q_OBJECT

    QJsonDocument configJSON;

private:
    /**
     * @brief Private CTOR to prevent instances of this class.
     *
     * @param parent
     */
    ConfigFacade(QObject *parent = nullptr);

public:
    /**
     * @brief Static method to get the singleton instance.
     *
     * @return the singleton @ConfigFacade
     */
    static ConfigFacade* getInstance()
    {
        static ConfigFacade facade;
        return &facade;
    }

    ~ConfigFacade();

public slots:
    /**
     * @brief Look up config value. If the given value is not available and a default is given,
     *        the value is added to the config document.
     *
     * @param group
     * @param key
     * @param defaultName
     *
     * @return the config value as a QVariant
     */
    QVariant getConfigValue(const QString& group, const QString& key, const QString& defaultName = "");

    bool storeConfig(const QString& configFile = "");
    bool loadConfig(const QString& configFile = "");
};
