#pragma once

#include <QObject>

namespace MyEnums
{
    Q_NAMESPACE

    enum class MyTestEnum {
        Test1,
        Test2,
        Test3
    };
    Q_ENUM_NS(MyTestEnum)

    enum class MyTestEnum2 {
        Test4,
        Test5,
        Test6
    };
    Q_ENUM_NS(MyTestEnum2)


};
