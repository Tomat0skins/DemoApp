<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>CustomItem1</name>
    <message>
        <location filename="build/mingw/MyApplication/qml/components/CustomItem1.qml" line="33"/>
        <location filename="qml/components/CustomItem1.qml" line="33"/>
        <source>My Component</source>
        <comment>Comment for translator</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyItemControls</name>
    <message>
        <location filename="build/mingw/FirstPlugin/MyItemControls.qml" line="18"/>
        <location filename="modules/FirstPlugin/MyItemControls.qml" line="18"/>
        <source>Text in an external module</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Page1</name>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page1.qml" line="12"/>
        <location filename="qml/Page1.qml" line="12"/>
        <source>Page 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page1.qml" line="32"/>
        <location filename="qml/Page1.qml" line="32"/>
        <source>Page One</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page1.qml" line="62"/>
        <location filename="qml/Page1.qml" line="62"/>
        <source>Loaded label</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Page2</name>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page2.qml" line="6"/>
        <location filename="qml/Page2.qml" line="6"/>
        <source>Page 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page2.qml" line="12"/>
        <location filename="qml/Page2.qml" line="12"/>
        <source>This is Page 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page2.qml" line="36"/>
        <location filename="qml/Page2.qml" line="36"/>
        <source>Go to Page 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page2.qml" line="43"/>
        <location filename="qml/Page2.qml" line="43"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page2.qml" line="83"/>
        <location filename="qml/Page2.qml" line="83"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Page3</name>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page3.qml" line="13"/>
        <location filename="qml/Page3.qml" line="13"/>
        <source>Page 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page3.qml" line="20"/>
        <location filename="qml/Page3.qml" line="20"/>
        <source>Page Three</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Page4</name>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page4.qml" line="11"/>
        <location filename="qml/Page4.qml" line="11"/>
        <source>Page 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page4.qml" line="152"/>
        <location filename="qml/Page4.qml" line="152"/>
        <source>First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page4.qml" line="160"/>
        <location filename="qml/Page4.qml" line="160"/>
        <source>Second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page4.qml" line="168"/>
        <location filename="qml/Page4.qml" line="168"/>
        <source>Third</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Page5</name>
    <message>
        <location filename="build/mingw/MyApplication/qml/Page5.qml" line="10"/>
        <location filename="qml/Page5.qml" line="10"/>
        <source>Page 6</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="build/mingw/MyApplication/qml/main.qml" line="19"/>
        <location filename="qml/main.qml" line="19"/>
        <source>&amp;Navigate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/main.qml" line="23"/>
        <location filename="qml/main.qml" line="23"/>
        <source>&amp;Page 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/main.qml" line="29"/>
        <location filename="qml/main.qml" line="29"/>
        <source>&amp;Page 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/main.qml" line="35"/>
        <location filename="qml/main.qml" line="35"/>
        <source>&amp;Page 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/main.qml" line="74"/>
        <location filename="qml/main.qml" line="74"/>
        <source>Page 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/main.qml" line="83"/>
        <location filename="qml/main.qml" line="83"/>
        <source>Page 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build/mingw/MyApplication/qml/main.qml" line="92"/>
        <location filename="qml/main.qml" line="92"/>
        <source>Page 3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
