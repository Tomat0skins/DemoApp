import QtQuick
import QtQuick.Controls

import Style
import ConfigFacade

/**
* Sample application window with menu, toolbar, side drawer etc.
*/
ApplicationWindow  {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("QML DemoApp: " + ConfigFacade.getConfigValue("application", "name", "Default Name"))

    menuBar: MenuBar {
        Menu {
            title: qsTr("&Navigate")

            MenuItem {
                id: menu1
                text: qsTr("&Page 1")
                onTriggered: stackView.push(page1)
            }

            MenuItem {
                id: menu2
                text: qsTr("&Page 2")
                onTriggered: stackView.push(page2)
            }

            MenuItem {
                id: menu3
                text: qsTr("&Page 3")
                onTriggered: stackView.push(page3)
            }

            MenuItem {
                id: menu4
                text: qsTr("&Page 4")
                onTriggered: stackView.push(page4)
            }

            MenuItem {
                id: menu5
                text: qsTr("&Page 5")
                onTriggered: stackView.push(page5)
            }

            MenuItem {
                id: menu6
                text: qsTr("&Page 6")
                onTriggered: stackView.push(page6)
            }
        }
    }

    header: ToolBar {
        anchors.top: menuBar.bottom
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }

    // example side drawer containing menu items
    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Page 1")
                width: parent.width
                onClicked: {
                    stackView.push(page1)
                    drawer.close()
                }
            }

            ItemDelegate {
                text: qsTr("Page 2")
                width: parent.width
                onClicked: {
                    stackView.push(page2)
                    drawer.close()
                }
            }

            ItemDelegate {
                text: qsTr("Page 3")
                width: parent.width
                onClicked: {
                    stackView.push(page2)
                    drawer.close()
                }
            }
        }
    }

    // pages are defined in seperate files
    Component {
        id: page1
        Page1 {
            // initialise and set published attritbute
            headerColor: Style.header.color.primary
        }
    }

    Component {
        id: page2
        Page2 {}
    }

    Component {
        id: page3
        Page3 {}
    }

    Component {
        id: page4
        Page4 {}
    }

    Component {
        id: page5
        Page5 {}
    }

    Component {
        id: page6
        Page6 {}
    }

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: page5
    }
}
