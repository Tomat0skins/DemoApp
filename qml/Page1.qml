import QtQuick
import QtQuick.Controls

import MyQuickItem
import FirstPlugin
import Style

// Page with some example QML items like ListView, Component, Loader etc.
// MyQuickItem is used to demonstrate C++ <==> QML communication via signals/slots
Page {
    id: page1
    title: qsTr("Page 1")

    // example how to alias internal properties to publish them
    property alias headerColor: label.color

    Rectangle {
        id: rect2
        width: parent.width; height: 30

        gradient: Gradient {
            GradientStop { position: 0.0; color: "lightsteelblue" }
            GradientStop { position: 1.0; color: "slategray" }
        }

        Label {
            id: label
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            color: Style.text.color.primary
            text: qsTr("Page One")
        }
    }

    // example how to use a loader to lazy initialise items
    Loader {
        id: loader1

        // Component comp1 is getting initialised (active = true) if MyItem2 is clicked
        sourceComponent: comp1
        active: false
    }

    // example for a component that is lazy initialised
    Component {
        id: comp1

        Rectangle {
            id: rect1
            width: parent.width; height: 30

            gradient: Gradient {
                GradientStop { position: 0.0; color: "lightsteelblue" }
                GradientStop { position: 1.0; color: "slategray" }
            }

            Label {
                id: label2

                anchors.horizontalCenter: parent.horizontalCenter
                color: Style.text.color.primary
                text: qsTr("Loaded label")
            }
        }
    }

    ListView  {
        id: list1
        anchors.top: rect2.bottom

        width: parent.width
        height: 100
        spacing: 2

        orientation: ListView.Horizontal

        // creates 10 * myItem
        model: 10
        delegate: myItem
    }

    Component {
        id: myItem

        MyItem {
            id: myItem1
            // example how to force property existens. If someone wants to create an
            // instance of this, index has to be defined
            required property int index

            width: 100; height: 100

            // communication example. Clicks onto the item are send to the
            // slot @MyQuickItem::onItemClicked
            // The item sends back a message in the signal onSendBack
            MyQuickItem {
                id: myItem2
                anchors.centerIn: parent
                width: 100; height: 100

                // example how to react on signals send from C++ item
                onSendBack: (message) => {
                                console.log("from C++ item: " + message);
                            }

                MouseArea {
                    anchors.fill: myItem2
                    onClicked: (mouse) => {
                                   console.log("clicked item " + index);

                                   // examples to call C++ item slot
                                   myItem1.onItemClicked();
                                   myItem2.onItemClicked(index);

                                   // acticate loader to display
                                   loader1.active = true;

                               }
                }
            }

            TextEdit {
                id: myEdit1
                text: index + " pos: " + list1.x
                onTextChanged: {
                    // example to call C++ item slot with text
                    myItem1.setText(myEdit1.text);
                }
            }
        }
    }
}
