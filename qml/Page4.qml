import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import CustomTableModel
import FirstPlugin

import Style

// Page contains an table with header and custom C++ model in a 3 way splitter
// Example usage of C++ bound properties (@see BindingTest)
Page {
    id: page4
    title: qsTr("Page 4")

    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal

        // example for an editable table with custom C++ model
        Rectangle {
            implicitWidth: 200
            SplitView.maximumWidth: 400
            color: "lightblue"

            // add headers to table
            HorizontalHeaderView {
                id: horizontalHeader
                syncView: table1
                anchors.left: table1.left
            }
            VerticalHeaderView {
                id: verticalHeader
                syncView: table1
                anchors.top: table1.top
            }

            TableView {
                id: table1
                anchors.fill: parent

                // scrollbars for the table
                ScrollBar.horizontal: ScrollBar{}
                ScrollBar.vertical: ScrollBar{}

                // show headers
                topMargin: 50
                leftMargin: 50

                columnSpacing: 3
                rowSpacing: 3
                clip: false

                model: CustomTableModel {
                    id: tableModel
                }

                selectionModel: ItemSelectionModel {
                    id: selections
                }

                delegate: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 50

                    color: selected ? Style.table.color.selected : Style.table.color.primary

                    required property bool selected
                    required property bool current

                    // display edit component on selection
                    Loader {
                        id: loader1
                        active: selected
                        sourceComponent: editComponent
                    }

                    // edit component, displayed if selected via loader
                    Component {
                        id: editComponent
                        TextEdit {
                            id: itemEdit
                            visible: selected

                            text: customrole
                            onTextChanged: {
                                console.log("Changed to: " + itemEdit.text);
                                var idx = tableModel.index(row,column);
                                if (tableModel.setData(idx, itemEdit.text)) {
                                    itemDisplay.text = itemEdit.text;
                                }
                            }
                        }
                    }

                    // display component if not selected
                    Text {
                        id: itemDisplay

                        visible: !selected
                        // the displayed text is defined via a custom role name
                        // this name is used in the CustomTableModel to get the dislayed text
                        // in the data() method
                        text: customrole
                    }


                    // listen for mouse to set selection
                    MouseArea {
                        id: mouseArea1
                        anchors.fill: parent
                        enabled: !selected
                        onClicked: {
                            var idx = tableModel.index(row,column);
                            console.log("Clicked cell: ", tableModel.data(idx));
                            selections.select(idx, ItemSelectionModel.Clear | ItemSelectionModel.Select);
                        }
                    }
                }

            }
        }

        // use the BindingTest class to display bound properties
        // no need to listen to signals, data changes get propagated automatically
        Rectangle {
            id: myBindingTest
            color: Style.background.color.secondary

            implicitWidth: 200
            Layout.maximumHeight: 100

            BindingTest {
                id: bindingTest
            }

            Label {
                text: qsTr("Bound Property: " + bindingTest.myVal + " counter: " + bindingTest.counter)
            }

        }

        // checkbox example with some styling and state change listening
        Rectangle {
            implicitWidth: 200
            color: "lightgreen"
            Rectangle {
                id: widget1
                anchors.fill: parent

                border.color: "yellow"
                border.width: 5
                radius: 10
                color: Qt.rgba(0, 0, 100, 0.5)

                ColumnLayout {
                    id: checkboxes
                    anchors.fill: parent

                    CheckBox {
                        Layout.leftMargin: 10

                        checked: true
                        text: qsTr("First")
                        onCheckStateChanged: {
                            console.log("First state: " + (checked ? "checked" : "not checked"));
                        }
                    }
                    CheckBox {
                        Layout.leftMargin: 20

                        text: qsTr("Second")
                        onCheckStateChanged: {
                            console.log("Second state: " + (checked ? "checked" : "not checked"));
                        }

                    }
                    CheckBox {
                        Layout.leftMargin: 30
                        text: qsTr("Third")
                        onCheckStateChanged: {
                            console.log("Third state: " + (checked ? "checked" : "not checked"));
                        }

                    }
                }
            }
        }
    }
}
