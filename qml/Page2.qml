import QtQuick
import QtQuick.Controls

// Page with example Action, Buttons, Dialog, page loading, State change, Animation etc.
Page {
    id: page2
    title: qsTr("Page 2")

    Column {
        spacing: 10
        Label {
            id: label1
            text: qsTr("This is Page 2")

            // animate the label by changing its x value
            NumberAnimation on x { to: 220; duration: 1000  }

            // State switch by clicking the parent
            MouseArea {
                 id: mouseArea
                 anchors.fill: parent
                 onClicked: {
                     label1.state === 'teststate' ? label1.state = "" : label1.state = 'teststate';
                 }
             }

            states: [
                State {
                     name: "teststate"
                     PropertyChanges { target: label1; text: "other state" }
                }
            ]
        }

        // page transition by loading a QML file directly
        Button {
            id: button1
            text: qsTr("Go to Page 3");
            onClicked: stackView.push("Page3.qml")
        }

        // calling an Action
        Button {
            id: button2
            text: qsTr("Copy");
            action: copyAction
        }

        // image from resources with a rotation animation
        Image {
            id: image1
            source: "qrc:/MyApplication/images/image.png"
            scale: 0.5
            opacity: 0.5
            asynchronous: true

            RotationAnimation on rotation {
                loops: Animation.Infinite
                from: 0
                to: 360
                duration: 1500
            }
        }
    }

    // Example of a dialog with button handling
    Dialog {
        id: dialog
        title: "Title"

        standardButtons: Dialog.Ok | Dialog.Cancel

        onAccepted: {
            console.log("Ok clicked");
        }

        onRejected: {
            console.log("Cancel clicked");
        }
    }

    // Action to be called
    Action {
        id: copyAction
        text: qsTr("&Copy")
        icon.name: "edit-copy"
        shortcut: StandardKey.Copy
        onTriggered: {
            console.log("copy clicked");
            dialog.visible = true;
        }
    }

}
