import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import com.test.enums
import FirstPlugin
import Controls
import Style

// Page with example tab bar. One side shows enums defined in C++ and QML. The second tab contains a dynamically filled ComboBox
Page {
    id: page5
    title: qsTr("Page 5")

    enum QMLEnum {
        Test7 = 7,
        Test8 = 8
    }

    // example tab bar with two tabs
    TabBar {
        id: bar
        width: parent.width

        TabButton {
            text: qsTr("Test")

        }

        TabButton {
            text: qsTr("Tree")

        }

        TabButton {
            id: button1
            text: qsTr("Enums")

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                border.color: bar.currentIndex === 0 ? "red" : "green"
                border.width: bar.currentIndex === 0 ? 2 : 1
                radius: bar.currentIndex === 0 ? 10 : 0
            }
        }

        TabButton {
            text: qsTr("ComboBox")
        }
    }

    StackLayout {
        width: parent.width
        currentIndex: bar.currentIndex

        anchors.top: bar.bottom

        Item {
            id: testTab

            TextEdit {
                id: user
                text: "user"
            }

            TextEdit {
                id: password
                text: "password"

                anchors.top: user.bottom
            }

            Button {
                text: qsTr("Login");
                anchors.top: password.bottom

                onClicked: {
                    console.log("login: " + user.text + " password: " + password.text);
                    // authHandler.signUserIn(user.text, password.text);
                }
            }

        }

        Item {
            id: anotherTab
            width: 200;

            MyButton {
                id: myButton1
                text: "Click me"
                width: 200;
                background {
                    radius: 5
                    color: "gray"
                }

                onClicked: {
                    myButton1.text = "clicked"
                }
            }

            MyButton {
                id: myButton2
                text: "Click me"
                width: 200;
                anchors.left: myButton1.right

                background {
                    radius: 5
                    color: "gray"
                }

                onClicked: {
                    myButton2.text = "clicked"
                }
            }

            MyLabel {
                id: myLabel1
                text: "trallala"

                anchors.left: myButton2.right
                width: 200;

                color: "yellow"
                background {
                    radius: 5
                    color: "blue"
                }

                onClicked: {
                    myLabel1.color = "red"
                    myLabel1.text = "clicked"
                }
            }

        }

        Item {
            id: enumTab

            // use the enums registered from C++
            Text {
                id: text1
                text: "Test: " + MyEnums.MyTestEnum.Test2
            }

            Text {
                id: text2
                anchors.top: text1.bottom
                text: "Test 2: " + MyEnums.MyTestEnum2.Test6
            }

            // use the enum defined in QML
            Text {
                id: text3
                anchors.top: text2.bottom
                text: "Test 3: " + Page5.QMLEnum.Test7
            }
        }

        Item {
            id: comboTab

            // example how to fill in entries into a ComboBox
            function fillModel() {
                for (var i = 0; i < 10; i++) {
                    model1.append({"key": "Item " + i, "value" : i});
                }
            }

            ComboBox {
                id: combo1
                textRole: "key"

                onActivated: console.log("selected " + combo1.currentIndex + ": " + combo1.currentText);

                Component.onCompleted: {
                    comboTab.fillModel();
                    combo1.currentIndex = 3;
                }
                model: ListModel {
                    id: model1
                }
            }
        }
    }
}
