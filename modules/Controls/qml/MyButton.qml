import QtQuick
import QtQuick.Controls

import Style

Item {
    id: myButtonBase

    // public

    property alias text: myButton.text
    property alias background: buttonBackground

    // signals

    signal clicked()

    // private

    Button {
        id: myButton

        padding: 2

        background: Rectangle {
            id: buttonBackground
            color: Style.text.color.secondary
        }

       onClicked: {
           myButtonBase.clicked();
       }
    }
}
