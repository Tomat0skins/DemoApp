#include <QtTest>

// add necessary includes here
#include "../../myitem2.h"

class TestCppSecondPlugin : public QObject
{
    Q_OBJECT

public:
    TestCppSecondPlugin();
    ~TestCppSecondPlugin();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();

};

TestCppSecondPlugin::TestCppSecondPlugin()
{

}

TestCppSecondPlugin::~TestCppSecondPlugin()
{

}

void TestCppSecondPlugin::initTestCase()
{

}

void TestCppSecondPlugin::cleanupTestCase()
{

}

void TestCppSecondPlugin::test_case1()
{
    MyItem2 item;
    item.onItemClicked();

    QVERIFY(true);
}

QTEST_APPLESS_MAIN(TestCppSecondPlugin)

#include "tst_testcpp2.moc"
