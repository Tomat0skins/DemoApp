#include <QtTest>

// add necessary includes here
#include "../../myitem.h"

class TestCppMyPlugin : public QObject
{
    Q_OBJECT

public:
    TestCppMyPlugin();
    ~TestCppMyPlugin();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();

};

TestCppMyPlugin::TestCppMyPlugin()
{

}

TestCppMyPlugin::~TestCppMyPlugin()
{

}

void TestCppMyPlugin::initTestCase()
{

}

void TestCppMyPlugin::cleanupTestCase()
{

}

void TestCppMyPlugin::test_case1()
{
    MyItem item;
    item.onItemClicked();

    QVERIFY(true);
}

QTEST_APPLESS_MAIN(TestCppMyPlugin)

#include "tst_testcpp2.moc"
