import QtQuick
import QtQuick.Controls

import QtTest

import FirstPlugin

TestCase {
    name: "TestQMLMyPlugin"

    function initTestCase() {
    }

    function cleanupTestCase() {
    }

    Component {
        id: component2

        MyItemControls {
            id: rect1
            componentBackground: "red"
        }

    }

    function test_case1() {        
        // component.module_function1();
        compare(1 + 1, 2, "sanity check");
        verify(true);
    }
}
