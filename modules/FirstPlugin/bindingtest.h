#pragma once

#include <qqml.h>

#include <QTimer>
#include <QObjectBindableProperty>

/**
 * @brief Example for bindung properties. On QML pages you can display the properties without
 *        listening to signals.
 */
class BindingTest : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_DISABLE_COPY(BindingTest)

    Q_PROPERTY(int myVal READ myVal WRITE setMyVal BINDABLE bindableMyVal)
    Q_PROPERTY(int counter READ counter WRITE setCounter BINDABLE bindableCounter)
public:
    BindingTest();

    int myVal() { return myValMember.value(); }
    void setMyVal(int newvalue) { myValMember = newvalue; }
    QBindable<int> bindableMyVal() { return &myValMember; }

    int counter() { return myCounter.value(); }
    void setCounter(int newvalue) { myCounter = newvalue; }
    QBindable<int> bindableCounter() { return &myCounter; }

signals:
    void myValChanged();
    void counterChanged();

private:
    Q_OBJECT_BINDABLE_PROPERTY(BindingTest, int, myValMember, &BindingTest::myValChanged);
    Q_OBJECT_BINDABLE_PROPERTY(BindingTest, int, myCounter, &BindingTest::counterChanged);
};

