import QtQuick
import QtQuick.Controls

import Style

Item {
    id: myItem
    property alias componentBackground: myRectangle.color

    Rectangle {
        id: myRectangle
        color: Style.background.color.secondary
        width: myItem.width

        height: myItem.height

        Label {
            text: qsTr("Text in an external module")
        }
    }
}
